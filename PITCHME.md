##### Exame de Julgamento
- São 6 cervejas a serem avalidadas em 90 minutos;
- Cada súmula é pontuada numa escala de 0 a 100 pontos.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#judging-exam

---
##### Nota < 60
- Habilidades reduzidas de degustação e avaliação;
- Súmulas com níveis inaceitáveis de:
  - completude;
  - informação descritiva;
  - e/ou *feedback*.
- Será um juiz **Apprentice**.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#exam-grading-guidelines

---
##### Nota 60+
- 60s:
  - Habilidades minimamente aceitáveis de julgamento e comunicação para um juiz **Recognized**.
- 70s:
  - Pelo menos 3/6 amostras avaliadas com precisão;
  - Informações descritivas e completude razoavelmente boas;
  - *Feedback* apropriado para um juiz **Certified**.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#exam-grading-guidelines

---
##### Nota 80+
- 80s:
  - Pelo menos 4/6 amostras avaliadas com precisão;
  - Súmulas com alta qualidade esperada de um juiz **National**.
- 90s:
  - Deve ser óbvio que o candidato possui experiência;
  - Pelo menos 5/6 amostras avaliadas com precisão;
  - Completude, informações descritivas e *feedback* em níveis **Master**.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#exam-grading-guidelines

---
##### Exame de Julgamento
- Cada súmula é pontuada numa escala de 0 a 100 pontos;
- Os pontos são divididos em 5 áreas de julgamento:
  - Precisão de Percepção (*Perception*);
  - Habilidade Descritiva (*Descriptive Ability*);
  - Comentários (*Feedback*);
  - Completude (*Completness and Communication*);
  - Precisão de Pontuação (*Scoring Accuracy*).

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#scoring-mechanics

---
##### Precisão de Percepção (20 pontos)
- Pontos são deduzidos por erros na percepção de aroma, aparência, sabor e sensação de boca;
- As súmulas dos *proctors* formam a assinatura à qual deve ser comparada a súmula do candidato.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#scoresheet-comments

---
##### Habilidade Descritiva (20 pontos)
- Descreva intensidade e características de aroma, aparência, sabor e sensação de boca;
- Utilize terminologia adequada;
- O Guia de Estilos BJCP serve como uma referência para este aspecto.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#scoresheet-comments

---
##### *Feedback* (20 pontos)
- O cervejeiro precisa receber *feedbacks* úteis e construtivos;
- Explique como ajustar a receita ou o procedimento para que a cerveja fique mais próxima do estilo;
- Comentários devem ser consistentes com as características percebidas pelo candidato e com a pontuação atribuída.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#scoresheet-comments

---
##### Completude (20 pontos)
- A súmula deve:
 - Ser legível e bem organizada;
 - Ter todo o espaço para comentários preenchido com conteúdo informativo;
- Marque os *checkboxes* para precisão, mérito técnico e intangíveis;
- Este aspecto da súmula está geralmente consistente com o nível de informação descritiva e *feedback*.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#scoresheet-comments

---
##### Precisão de Pontuação (20 pontos)
- A pontuação do candidato é comparada com a dos *proctors*.

http://dev.bjcp.org/exam-certification/exam-grading/exam-scoring-guide/#scoring-accuracy

---
##### Exame de Julgamento
- Pontos recebidos em cada área de julgamento estão relacionados com os níveis de experiência:
 - 12-13 seria o esperado de um juiz **Recognized**;
 - 14-15 de um juiz **Certified**;
 - 16-17 de um juiz **National**;
 - 18-20 de um juiz **Master**.
- Desempenho não satisfatório geralmente se encontra na faixa dos 9-11 pontos.

https://www.bjcp.org/docs/Scoring_Guide.doc

---
##### Precisão de Percepção
- Requer a síntese de dados de várias fontes em uma representação concisa das características de cada cerveja:
 - Súmulas dos *proctors*;
 - Súmulas dos participantes do exame;
 - Descrições das cervejas fornecidas pelo organizador do exame;
 - Guia de Estilos do BJCP.

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception

---
##### Habilidade de Percepção
- Uma mesma característica pode ser percebida de forma distinta por diferentes pessoas:
 - *grainy*, *husky*, *grassy* - são *flavors* vizinhos na [**Beer Flavor Wheel**](http://www.beerflavorwheel.com/).
- Se o candidato omite o nível de amargor num estilo em que este atributo é desejável
  este erro deve ser contabilizado tanto em **Percepção** quanto em **Descrição**.

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

---
##### Avaliação de habilidades de percepção
- Recapitule o estilo avaliado no Guia de Estilos;

- Reveja informações fornecidas pelo organizador:
 - É um exemplo clássico do estilo?
 - Foi adulterado?
 - Era um exemplar envelhecido?

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

---
##### Avaliação de habilidades de percepção
- Examine a súmula do *proctor* mais experiente;

- Anote descritores-chave percebidos para aroma:
 - Malte, lúpulo e outros;
 - Atributos dentro e fora do esperado;
 - Pelo menos de baixa a moderada intensidade;
 - Capture 3 ou 4 atributos primários e secundários.

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

---
##### Avaliação de habilidades de percepção
- Anote descritores-chave percebidos para aparência:

 - Particularmente os não apropriados para o estilo;

 - turbidez, ou cor muito escura ou clara...

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

---
##### Avaliação de habilidades de percepção
- Anote descritores-chave percebidos para sabor:
 - Malte, lúpulo, equilíbrio e outros;
 - Atributos dentro e fora do esperado;
 - Pelo menos de baixa a moderada intensidade;
 - Capture 3 ou 4 atributos primários e secundários.

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

---
##### Avaliação de habilidades de percepção
- Anote descritores-chave percebidos para sensação de boca;

- Acrescente descritores identificados pelos outros *proctors*;

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

---
##### Avaliação de habilidades de percepção
- Reduza a lista de descritores dos *proctors*:
 - 6 a 8 características primárias e secundárias;
 - São características que capturam a essência da cerveja;
 - Espera-se que sejam encontradas nas súmulas dos candidatos.

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

---
##### Avaliação de habilidades de percepção
10. Examine as súmulas dos candidatos:
 - Considere como parte da lista características encontradas por pelo menos metade dos candidatos.

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-ability

Note:

    Se o candidato anotou uma característica que não estava entre os
    descritores dos *proctors*, é possível que este seja um erro de percepção,
    mas também pode ser que ela entre pra matriz de descritores se uma
    significativa fração de candidatos anotaram esta mesma característica.
    Nesse caso é apropriado dar crédito para os candidatos que notaram
    esta característica, mas sem deduzir dos candidatos que não a notaram.

---
##### Competências de Percepção evidentes em súmulas de juízes **Master** e **Grand Master**
- Componentes primários de aroma são consistentes;
- Não há erros de percepção significativos no aroma;
- Descrições de cor, limpidez e colarinho são consistentes;
- Componentes primários de sabor são consistentes;
- Componentes primários de sensação de boca são consistentes.

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-competencies

Note:

    Componentes primários de aroma, sabor e sensação de boca são consistentes com o
    consenso entre os *proctors* e/ou mais que a metade dos candidatos. Não existem
    componentes de aroma em nível baixo ou maior que não estão incluídos no consenso
    entre os *proctors* e/ou mais que a metade dos candidatos. É desejável descrever
    outras características do colarinho, mas esta informação não é essencial para o
    cervejeiro.

---
##### Rubrica de Percepção (20 pontos)

- Não mais que 2 elementos diferem: Master (18-20);
- Não mais que 3 elementos diferem: National (16–17);
- Não mais que 4 elementos diferem: Certified (14–15);
- Não mais que 5 elementos diferem: Recognized (12–13);
- 5 ou mais elementos diferem: Apprentice (10–11);
- Não há comentários o suficiente: Mínimo (8–9).

http://dev.bjcp.org/exam-certification/exam-grading/bjcp-scoresheet-guide/#perception-rubric

---
##### Percepção nível Master (Ted Hausotter, 11/28/2010)

- Necessário ter precisão: pratique!
- Sempre cite as características básicas do estilo:
 - Presentes ou não.
- Saiba como os *flavors* interagem entre si;
- Sensação escorregadia nem sempre é diacetil!
 - É uma dica para procurar por diacteil, não um determinante.

http://66.147.244.69/~bjcporg/cms/wp-content/uploads/2012/02/MasterSheet.pdf

---
##### Habilidade Descritiva

- O juíz deve ser capaz de descrever tanto quantidade quanto qualidade de componentes de aroma e sabor.

---
##### Exemplo de descrições de uma APA
- Aprendiz: “aroma lupulado”;
- Recognized: “aroma de lúpulo americano”;
- Certified: “aroma moderado de lúpulo americano”;
- National: “aroma moderado de lúpulo cítrico americano”;
- Master: “aroma moderado de lúpulo cítrico americano, com notas de tangerina e laranja.”

---
##### Competências de habilidades descritivas nível Master

- Mínimo de adjetivos[1] e/ou frases[2] descritivas para cada item:
 - Aroma: 5;
 - Intensidade de aroma: 3;
 - Aparência: 3;
 - Sabor: 5;
 - Intensidade de sabor: 3;
 - Sensação de boca: 3.
 
---
##### Competências de habilidades descritivas nível Master
 
- [1] Palavras mais específicas do que as genéricas (maltado, lupulado, frutado).
 - “Sem” ou “Nenhum” são adjetivos válidos para características que seriam apropriadas no estilo julgado, mas não estão presentes.
- [2] “Um sabor de lúpulo nobre condimentado aparece no meio-de-boca,” possui:
 - 2 adjetivos descritivos para sabor de lúpulo;
 - 1 para intensidade;
 - 1 frase descritiva.

---
##### Rubrica de habilidades descritivas (20 pontos)

 - 20 ou mais adjetivos/frases: Master (18-20);
 - 18-19 adjetivos/frases: National (16–17);
 - 15-17 adjetivos/frases: Certified (14–15);
 - 12-14 adjetivos/frases: Recognized (12–13);
 - 9-11 adjetivos/frases: Apprentice (10–11);
 - menos de 9 adjetivos/frases: Mínimo (8–9).

---
##### Habilidades descritivas nível Master (Ted Hausotter, 11/28/2010)

Descrições devem:

- Ser fáceis de entender;
- Possuir termos que todos conhecem;
- Ter adjetivos além dos *flavors* básicos (ex.: lúpulo x lúpulo cítrico);
- Mencionar intensidade (ex.: lúpulo x lúpulo moderado);
- Possuir vários descritores;
- Sempre mencionar as características que definem o estilo;
- Permitir que o leitor da súmula “vivencie” a cerveja.

http://66.147.244.69/~bjcporg/cms/wp-content/uploads/2012/02/MasterSheet.pdf

---

##### Erros comuns que devem ser evitados

- Não descrever a cerveja na sua frente:
 - O juiz simplesmente transcreve para a súmula, mais ou menos, o que está no Guia de Estilo.
- Usar termos vagos como “boa”, “legal”;
- Não mencionar a intensidade de cada descritor;
- Só mencionar descritores primários;

---
##### Exemplo de evolução de descritores

- Expansão de maltado”:
 - Primário: “maltado”;
 - Secundário: “malte *grainy*”;
 - Terciário: “malte *grainy-sweet* com notas de mel”.
- Expansão de frutado”:
 - Primário: “frutado”;
 - Secundário: “frutas de caroço”;
 - Terciário: “pêssegos e ameixas frescas”.

http://dev.bjcp.org/education-training/exam-preparation/beer-part-2-perception-descriptive-ability/

---
